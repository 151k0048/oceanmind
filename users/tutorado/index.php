<?php
  session_start();

    if(isset($_SESSION['usuario'])){
       if($_SESSION['usuario']['UsuarioTipo'] == 2 ){
            header('Location: ../coordinador/');
        }else if($_SESSION['usuario']['UsuarioTipo'] == 3 ){
            header('Location: ../tutor/');
        }else if($_SESSION['usuario']['UsuarioTipo'] == 1 ){
            header('Location: ../administrador/');
        }
    }else{
            header('location: ../../../');
        }  

        $sesionActual = $_SESSION['usuario']['IDUsuario'];
    try{
        require_once("../../php/conexion.php"); //enlazar el archivo de conexion
        $consultas = mysqli_query($mysqli,"SELECT * FROM tutorado WHERE IDUsuario = '$sesionActual' ");
        }catch(Exception $e){
            $error = $e->getMessage(); //usar funcion de mysqli para capturar el error e imprimirlo
        }
        
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Orienta·U</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- Font Awesome CSS-->
       <link rel="stylesheet" href="../../vendor/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="../../vendor/fontawesome5/css/all.css">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

      <link rel="stylesheet" href="../../vendor/fonts/Poppins.zip">
      <link rel="stylesheet" href="../../vendor/fonts/Lato-Regular.ttf">

    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../../css/fontastic.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="../../css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="../../vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../../css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    
    <!-- Favicon-->
    <link rel="shortcut icon" href="../../img/logoweb.png">
    
     
    <!--css slider normateca-->
    
    
    <link rel="stylesheet" href="../../css/styledash.css">
    
  </head>
  <body>
    <!-- Side Navbar -->
    <nav class="side-navbar">
      <div class="side-navbar-wrappr animated slideInLeft">
       <div class=" logo-principal sidenav-header-inner" style="">
           <a href="general/general.php"><img src="../../img/logoweb.png" class="img-fluid" alt=""></a>
       </div>
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          
          <!-- User Info-->
          <div class="sidenav-header-inner text-center"><img src="../../img/avatar-1.jpg" alt="person" class="img-fluid rounded-circle">
            <h2 class="letra_usuario"><?php echo $_SESSION['usuario']['Nombres'] ?> <?php echo $_SESSION['usuario']['ApellidoP'] ?></h2>
          </div>
          <!-- Small Brand information, appears on minimized sidebar-->
          <div class="sidenav-header-logo"><a href="../index.php" class="brand-small" > <strong>O·</strong><strong class="">U</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu animated slideInLeft">
          <hr class="sidenav-heading  justify-content-center">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="index.php" > <i class="far fa-user"></i>Identificación</a></li>
            <li><a href="desarrollo-humano/index.php" aria-expanded="false"> <i class="far fa-smile-wink"></i>Desarrollo Humano </a>
              
            </li>
            <li><a href="habilidades-pensamiento/index.php" aria-expanded="false" > <i class="far fa-lightbulb fa-2x"></i>Pensamiento </a>
              
            </li>
            <li><a href="fortalecimiento/index.php" aria-expanded="false" > <i class="fas fa-dumbbell"></i>Fortalecimiento </a>
              
            </li>
            <li><a href="general/normateca-books.php" aria-expanded="false" > <i class="fas fa-book"></i>Material de Lectura </a>
              
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="page">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header "><a id="toggle-btn" href="#" class="menu-btn"><i class="fas fa-bars "> </i></a><a href="index.html" class="navbar-brand">
                  <div class="brand-text d-none d-md-inline-block"></div></a></div>
                  
                  <!--Migas de pan identificación-->
                  
                  <div class="col align-self-start">
                    <ol class="breadcrumb" style="background: none; color: white; width: auto; height: 30px; font-family: lato; ">
                        <li>
                            <a style="font-size: 20px;" href="general/general.php">
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        <div style="font-size: 20px;font-family:lato">&nbsp; > &nbsp;</div>
                        <li class="active">
                            <a style="font-size: 20px;font-family:lato">
                                <u>Identificación</u>
                            </a>
                        </li>
                    </ol>
                </div> 
                  
                  
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                
                <!-- Notificaciones dropdown-->
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell fa-2x"></i><span class="badge contadornoti">8</span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu shadow p-3 mb-5 bg-white rounded dropdown-menu-right navbar-dropdown animated bounceInDown">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
                          <div class="notification-time"><small>10 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>
                  </ul>
                </li>
                
              
                <!-- Log out-->
                          <li class="nav-item dropdown  ">
            <a class="nav-link dropdown-toggle" id="notifications" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
              <img class="img-xs rounded-circle" src="../../img/avatar-1.jpg" alt="Profile image" width="37px;" height="37px" style="margin-top: -8px">
            </a>
            <div class="dropdown-menu shadow p-3 mb-5 rounded dropdown-menu-right navbar-dropdown animated bounceInDown" aria-labelledby="UserDropdown">
                <a class="dropdown-item" href="general/perfil.php">
                <i class="fas fa-user"></i><span>Perfil</span>
              </a>
         
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="../salir.php">
                <i class="fas fa-sign-out-alt"></i><span>Salir</span>
              </a>
            </div>
          </li>
                
              </ul>
            </div>
          </div>
        </nav>
      </header>
     <!--Inicio de Preguntas-->
     
       
     
      <!--Fin de Preguntas-->
      
       <div class="row justify-content-center mt-5 col-sm-12 ml-1 mr-1 container">
           <div class="col-md-10 col-sm-12">
              <div class="">
              <div class="card-header d-flex justify-content-center ">
                 
      <h2 class="animated bounce titulo-dash">¡Arrancando motores!</h2>
                </div>
                    
                <div class="card-body">
                 
        <div class="container d-flex justify-content-center ">
         <?php 
                  echo '<div class="main-wrap">';
                  
                  
                  $ConsultaTutorado = mysqli_query($mysqli,"SELECT * FROM tutorado WHERE IDUsuario = '$sesionActual' ");
                  $data=mysqli_fetch_assoc($ConsultaTutorado);
                  $tutorado = $data['IDTutorado'];

                  $ConsultaIdentificacion = mysqli_query($mysqli,"SELECT * FROM `ficha-identificacion` WHERE IDTutorado = '$tutorado' ");
                  $data2=mysqli_fetch_assoc($ConsultaIdentificacion);
                  $tutoradoID = $data2['IDTutorado'];

                  $ConsultaFODA = mysqli_query($mysqli,"SELECT * FROM `foda` WHERE IDTutorado = '$tutorado' ");
                  $data3=mysqli_fetch_assoc($ConsultaFODA);
                  $tutoradoFoda = $data3['IDTutorado'];
                  $fortaleza1 = $data3['Fortaleza1'];
                  $oportunidad1 = $data3['Oportunidad1'];
                  $debilidad1 = $data3['Debilidad1'];
                  $amenaza1 = $data3['Amenaza1'];

                  $ConsultaLV = mysqli_query($mysqli,"SELECT * FROM `linea-vida` WHERE IDTutorado = '$tutorado' ");
                  $data4=mysqli_fetch_assoc($ConsultaLV);
                  $tutoradoLV = $data4['IDTutorado'];
                
                  $ConsultaLV = mysqli_query($mysqli,"SELECT * FROM `resultado-estilos-aprendizaje` WHERE IDTutorado = '$tutorado' ");
                  $data4=mysqli_fetch_assoc($ConsultaLV);
                  $tutoradoEA = $data4['IDTutorado'];

                  //Iddentificacion
                  if ($tutorado = $tutoradoID) {
                      echo '<div class="step-progress">
                  <br>
                  <div class="step active">
                        <a href="identificacion/datos-personales.php"><div class="point" onclick="toggle(this)" id="myButton"><i class="fas fa-id-card">
                        </i><p class="titulo-point">Identificación</p></div>                        
                      </a></div>';
     
                  }else{
                        echo '<div class="step-progress">
                        <br>
                        <div class="step">
                        <a href="identificacion/datos-personales.php"><div style="color: #31C6C7;" class="point" onclick="toggle(this)" id="myButton"><i class="fas fa-id-card">
                        </i><p class="titulo-point">Identificación</p></div>                        
                      </a></div>';
                      }
                //FODA                
                  if ($tutorado = $tutoradoID) {
                      if($tutorado = $tutoradoFoda){echo '<div class="step active">
                            <div class="progress"></div>
                          <a href="identificacion/foda.php"><div class="point" ><i class="fas fa-puzzle-piece"></i><p class="titulo-point">FODA</p></div>
                          </a></div>
                      ';}
                      else{echo '<div class="step ">
                            <div class="progress"></div>
                          <a href="identificacion/foda.php"><div style="color: #31C6C7;" class="point" ><i class="fas fa-puzzle-piece"></i><p class="titulo-point">FODA</p></div>
                          </a></div>
                      ';}
                  }
                  else{
                      echo '<div class="step">
                            <div class="progress"></div>
                          <a /*href="avisoIdentificacion.php"*/ href="identificacion/foda.php"><div style="color: #31C6C7;" class="point" ><i class="fas fa-puzzle-piece"></i><p class="titulo-point">FODA</p></div>
                          </a></div>
                      ';
                  }
                
                
                //LineaVida
                if (!empty($fortaleza1 != "" && $oportunidad1 != "" && $debilidad1 != "" && $amenaza1 != "")) {
                          if($tutorado = $tutoradoLV){
                              echo '
                            <div class="step active">
                              <div class="progress"></div>
                              <a href="identificacion/linea-vida.php"><div class="point" ><i class="fas fa-heartbeat "></i><p class="titulo-point">Linea de Vida</p></div>
                            </a></div>
                          ';
                          
                      }
                      else{
                      
                           echo '
                            <div class="step">
                              <div class="progress"></div>
                              <a href="identificacion/linea-vida.php"><div style="color: #31C6C7;" class="point" ><i class="fas fa-heartbeat "></i><p class="titulo-point">Linea de Vida</p></div>
                            </a></div>';
                      
                      }
                  }
                  else{
                      echo '
                        <div class="step">
                              <div class="progress"></div>
                              <a /*href="avisoIdentificacion.php"*/ href="identificacion/linea-vida.php"><div style="color: #31C6C7;" class="point" ><i class="fas fa-heartbeat "></i><p class="titulo-point">Linea de Vida</p></div>
                            </a></div>';  
                  }
                
                //EstilosAprendizaje                
                if ($tutorado = $tutoradoLV) {
                      if($tutorado = $tutoradoEA){
                          
                          echo '
                                <div class="step active">
                                      <div class="progress"></div>
                                      <a href="identificacion/estilos-aprendizaje.php"><div class="point" ><i class="fas fa-book-open"></i><p class="titulo-point">Estilos Aprendizaje </p></div>
                                    </a></div>
                              ';
                      }
                      else{
                      
                           echo '
                                <div class="step">
                                      <div class="progress"></div>
                                      <a href="identificacion/estilos-aprendizaje.php"><div class="point" ><i style="color: #31C6C7;" class="fas fa-book-open"></i><p class="titulo-point">Estilos Aprendizaje </p></div>
                                    </a></div>
                              ';
                      
                      }
                  }
                  else{
                     echo '
                                <div class="step">
                                      <div class="progress"></div>
                                      <a /*href="avisoIdentificacion.php"*/ href="identificacion/estilos-aprendizaje.php"><div style="color: #31C6C7;" class="point" ><i class="fas fa-book-open"></i><p class="titulo-point">Estilos Aprendizaje </p></div>
                                    </a></div>
                              '; 
                  }
                
                  
                  
                  



echo '</div>'; 


  
 
echo'</div>';
                      
                
            ?>

</div>
              </div>
            </div>
           </div>
           </div>
     
      
       
      
      <footer class="main-footer  mt-3 col-md-12">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
               

            </div>
            <div class="col-md-12 d-flex justify-content-center">
                <i class="fab fa-facebook fa-2x colorfooter"  aria-hidden="true"></i>
                      <i class="fab fa-twitter fa-2x"   aria-hidden="true"></i>
                      <i class="fab fa-youtube fa-2x "    aria-hidden="true"></i>
                      <i class="fab fa-instagram fa-2x"  aria-hidden="true"></i>
            </div>
           
          </div>
        </div>
      </footer>
    </div>
    <!-- JavaScript files-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>
    
    <script src="../../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../../js/wow.min.js"></script>
    <script src="../../vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js'></script>
    <script src="../../vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../../js/indexdash.js"></script>
    <script src='https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js'></script>
    
    <script>
      new WOW().init();
      </script>
    <!-- Main File-->
    <script src="../../js/front.js"></script>
  </body>
</html>